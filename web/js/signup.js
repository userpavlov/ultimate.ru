$('document').ready(function () {
    //маски для ввода электронной телефона
    $(".phone_mask").mask("+7(999)999-99-99");

    //Кастомный валидатор для проверки паролей
    $.validator.addMethod("passwordChars", function (value, element, param) {
        var validated =  true;
        if(!/\d/.test(value))
            validated = false;
        if(!/[a-z]/.test(value))
            validated = false;
        if(!/[A-Z]/.test(value))
            validated = false;
        if(!/[!@#$%^&*]/.test(value))
            validated = false;

        return validated;
    }, "Пароль должен содержать буквы обоих регистров, знаки препинания и цифры");

    /* Валидация формы на стороне клиента */
    $("#register-form").validate({
        rules:
            {
                username: {
                    required: true,
                },
                surname: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true,
                    // minlength: 11,
                    //  maxlength: 11
                },
                address: {
                    required: true
                },
                password: {
                    required: true,
                    minlength: 8,
                    maxlength: 15,
                    equalTo: "#confirmSignupPassword",
                    passwordChars: true
                },
                password_confirm: {
                    required: true,
                    minlength: 8,
                    maxlength: 15,
                    passwordChars: true
                },
            },
        messages:
            {
                username: {
                    required: "Введите имя пользователя",
                },
                surname: {
                    required: "Введите фамилию",
                },
                email: {
                    required: "Введите электронную почту",
                    email: "Некорректная электронная почта"
                },
                phone: {
                    required: "Введите телефон",
                    //minlength: "В телефоне должно быть не меньше 11 цифр",
                    //maxlength: "В телефоне должно быть не больше 11 цифр",
                },
                address: {
                    required: "Введите адрес"
                },
                password: {
                    required: "Введите пароль",
                    minlength: "Пароль должен быть не меньше 8 символов",
                    maxlength: "Пароль должен быть длиннее 15 символов",
                    equalTo: "Пароли не совпадают",
                },

            },
        submitHandler: submitForm,

        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });


    /* validation */

    /* form submit */
    function submitForm() {
        var data = $("#register-form").serialize();

        $.ajax({

            type: 'POST',
            url: '/admin/auth/signup.php',
            data: data,
            beforeSend: function () {
                $("#error").fadeOut();
                $("#btn-submit").html('<span class="glyphicon glyphicon-transfer"></span>   sending ...');
            },
            success: function (data) {
                if (data.trim() != '') {
                    $('.errors').html("<div class='alert alert-danger'>" + data + "</div>");
                }
                else {
                    $('#signupModal').hide();
                    window.location.reload();
                }
            }

        });
        return false;
    }

    /* form submit */

});