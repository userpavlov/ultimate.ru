$('document').ready(function () {

    /* Валидация формы на стороне клиента */
    $("#login-form").validate({
        rules:
            {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                },
            },
        messages:
            {
                email: {
                    required: "Введите электронную почту",
                    email: "Некорректная электронная почта"
                },
                password: {
                    required: "Введите пароль",
                },
            },
        submitHandler: submitForm,

        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });

    /* Валидация */

    /* Отиправка формы */
    function submitForm() {
        var data = $("#login-form").serialize();

        $.ajax({

            type: 'POST',
            url: '/admin/auth/login.php',
            data: data,
            success: function (data) {
                if (data.trim() != '') {
                    $('.errors').html("<div class='alert alert-danger'>" + data + "</div>");
                }
                else {
                    $('#loginModal').hide();
                    window.location.reload();
                }
            }
        });
        return false;
    }

    //Выход
    $("#logoutButton").click(function(){
        $.ajax({
            type: 'POST',
            url: '/admin/auth/logout.php',
            success: function (data) {
                    window.location.reload();
            }
        });
    });

});