<div class="container">
    <h1>Данные пользователя</h1>

    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Профиль</a></li>
        <li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Редактирование профиля</a></li>
        <li role="presentation"><a href="#tab3" role="tab" data-toggle="tab">Смена пароля</a></li>
    </ul>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="tab1">
            <p>
            <ul class="list-unstyled">
                <li><span class="glyphicon glyphicon-user"></span>  Имя: <?= $user->username ?></li>
                <li><span class="glyphicon glyphicon-user"></span>  Фамилия: <?= $user->surname ?></li>
                <li><span class="glyphicon glyphicon-envelope"></span> Почта: <?= $user->email ?></li>
                <li><span class="glyphicon glyphicon-earphone"></span> Телефон: <?= $user->phone ?></li>
                <li><span class="glyphicon glyphicon-home"></span> Адрес: <?= $user->address ?></li>
            </ul>
            </p>
        </div>
        <div role="tabpanel" class="tab-pane" id="tab2">
            <p>
            <form class="user-edit-form1" action="/admin/user/view.php?id=<?= $user->id ?>" method="post">
                <div class="form-group">
                    <label class="control-label" for="username">Имя пользователя</label>
                    <input type="text" name="username" id="username2" tabindex="1" class="form-control"
                           placeholder="" value="<?= $user->username ?>">
                </div>

                <div class="form-group">
                    <label class="control-label" for="surname">Фамилия</label>
                    <input type="text" name="surname" id="surname2" tabindex="1" class="form-control"
                           placeholder="" value="<?= $user->surname ?>"">
                </div>
                <div class="form-group">
                    <label class="control-label" for="phone">Телефон</label>
                    <input type="phone" name="phone" id="phone2" tabindex="3"
                           class="form-control phone_mask"
                           placeholder="" value="<?= $user->phone ?>">
                </div>
                <div class="form-group">
                    <label class="control-label" for="address">Адрес</label>
                    <input type="address" name="address" id="address2" tabindex="4" class="form-control"
                           placeholder=" " value="<?= $user->address ?>">
                </div>

                <div class="form-group">
                    <input type="submit" name="register-submit" id="update-submit2"
                           tabindex="7" class="form-control btn btn-primary btn-register"
                           value="Сохранить">
                </div>
            </form>
        </p>
        </div>


        <div role="tabpanel" class="tab-pane" id="tab3">
            <form class="user-edit-form2" action="/admin/user/view.php?id=<?= $user->id ?>" method="post">
                <div class="form-group">
                    <label class="control-label" for="password">Пароль</label>
                    <input type="password" name="password" id="signupPassword2" tabindex="5"
                           class="form-control" placeholder="" autocomplete="off">
                </div>
                <div class="form-group">
                    <label class="control-label" for="password_confirm">Пароль подтверждения</label>
                    <input type="password" name="password_confirm" id="confirmSignupPassword2"
                           tabindex="6"
                           class="form-control " placeholder="" autocomplete="off">
                </div>

                <div class="form-group">
                    <input type="submit" name="register-submit" id="update-submit3"
                           tabindex="7" class="form-control btn btn-primary btn-register"
                           value="Сохранить">
                </div>
            </form>
        </div>
    </div>


</div>
<script>
    $('document').ready(function () {

        $(".user-edit-form1").validate({
            rules:
                {
                    username: {
                        required: true,
                    },
                    surname: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    phone: {
                        required: true,
                        // minlength: 11,
                        //  maxlength: 11
                    },
                    address: {
                        required: true
                    },
                },
            messages:
                {
                    username: {
                        required: "Введите имя пользователя",
                    },
                    surname: {
                        required: "Введите фамилию",
                    },
                    email: {
                        required: "Введите электронную почту",
                        email: "Некорректная электронная почта"
                    },
                    phone: {
                        required: "Введите телефон",
                        //minlength: "В телефоне должно быть не меньше 11 цифр",
                        //maxlength: "В телефоне должно быть не больше 11 цифр",
                    },
                    address: {
                        required: "Введите адрес"
                    },
                },

            errorElement: "em",
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help-block");

                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"));
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });

        $(".user-edit-form2").validate({
            rules:
                {
                    password: {
                        required: true,
                        minlength: 8,
                        maxlength: 15,
                        equalTo: "#confirmSignupPassword2",
                        passwordChars: true
                    },
                    password_confirm: {
                        required: true,
                        minlength: 8,
                        maxlength: 15,
                        passwordChars: true
                    },
                },
            messages:
                {
                    password: {
                        required: "Введите пароль",
                        minlength: "Пароль должен быть не меньше 8 символов",
                        maxlength: "Пароль должен быть длиннее 15 символов",
                        equalTo: "Пароли не совпадают",
                    },

                },

            errorElement: "em",
            errorPlacement: function (error, element) {
                // Add the `help-block` class to the error element
                error.addClass("help-block");

                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.parent("label"));
                } else {
                    error.insertAfter(element);
                }
            },
            highlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
            }
        });
    });

</script>