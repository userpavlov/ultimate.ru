<div class="user-list">
    <h2>Список пользователей</h2>
    <table class="table table-striped">
        <thead class="thead-dark">
        <tr>
            <th scope="col"></th>
            <th scope="col">Фамилия</th>
            <th scope="col">Имя</th>
            <th scope="col">Почта</th>
            <th scope="col">Телефон</th>
            <th scope="col">Адрес</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <?php $i = 0; ?>
        <?php foreach ($users as $user): ?>
            <tr>
                <th scope="row"><?= ++$i ?></th>
                <td><?= $user->surname ?></td>
                <td><?= $user->username ?></td>
                <td><?= $user->email ?></td>
                <td> <?= $user->phone ?></td>
                <td><?= $user->address ?></td>
                <td><a href="/admin/user/view?id=<?= $user->id ?>"</td>
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="text-center">
        <?= $pagination->getAjaxTags() ?>
    </div>
</div>