﻿<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Демонстрационная</title>

    <!-- Bootstrap -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="/js/jquery/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/js/jquery-validation-1.19.0/dist/jquery.validate.js"></script>
    <script src="/js/jquery-maskedinput/jquery.maskedinput.min.js"></script>


    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/signup.js"></script>
    <script type="text/javascript" src="/js/login.js"></script>
</head>
<body>
<div id="content">
    <!-- Классы navbar и navbar-default -->
    <nav class="navbar navbar-default navbar-static-top">
        <!-- Контейнер -->
        <div class="container-fluid">
            <!-- Заголовок -->
            <div class="navbar-header">
                <!-- Бренд или название сайта -->
                <a class="navbar-brand" href="#">Демонстрационный MVC-фреймворк</a>
            </div>
            <!-- Основная часть меню -->
            <div class="collapse navbar-collapse" id="navbar-main">
                <!-- Содержимое основной части -->
                <ul class="nav navbar-nav pull-right">
                    <?php if (Application::$user->isGuest()): ?>
                        <li><a href="#" data-toggle="modal" data-target="#loginModal">Вход</a></li>
                    <?php endif; ?>
                    <?php if (!Application::$user->isGuest()): ?>
                        <li><a href="#" id="logoutButton">Выход</a></li>
                    <?php endif; ?>
                    <?php if (Application::$user->isGuest()): ?>
                        <li><a href="#" data-toggle="modal" data-target="#signupModal">Регистрация</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">

            <li class="breadcrumb-item"><a href="<?= 'http://' . $_SERVER['HTTP_HOST'] ?>">Пользователи</a></li>
            <?php if (isset($breadcrumbs)): ?>
                <?php for ($i1 = 0; $i < count($breadcrumbs); $i++): ?>
                    <li class="breadcrumb-item active"><?= $breadcrumbs[$i1] ?></li>
                <?php endfor; ?>
            <?php endif; ?>
        </ol>
    </nav>

    <div class="container">
        <?= $content ?>
    </div>
</div>

<!-- Модальное окно "Регистрация"-->
<div class="modal" id="signupModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- заголовок -->
            <div class="modal-header">
                <h3 class="modal-title">Регистрация</h3>
            </div>

            <!-- Контент формы -->
            <div class="modal-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form id="register-form" method="post">
                                <div class="errors"></div>
                                <div class="form-group">
                                    <label class="control-label" for="username">Имя пользователя</label>
                                    <input type="text" name="username" id="username" tabindex="1" class="form-control"
                                           placeholder="" value="">
                                </div>

                                <div class="form-group">
                                    <label class="control-label" for="surname">Фамилия</label>
                                    <input type="text" name="surname" id="surname" tabindex="1" class="form-control"
                                           placeholder="" value="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="email">Электронная почта</label>
                                    <input type="email" name="email" id="email" tabindex="2"
                                           class="form-control mail_mask"
                                           placeholder="" value="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="phone">Телефон</label>
                                    <input type="phone" name="phone" id="phone" tabindex="3"
                                           class="form-control phone_mask"
                                           placeholder="" value="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="address">Адрес</label>
                                    <input type="address" name="address" id="address" tabindex="4" class="form-control"
                                           placeholder=" " value="">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="password">Пароль</label>
                                    <input type="password" name="password" id="signupPassword" tabindex="5"
                                           class="form-control" placeholder="" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="password_confirm">Пароль подтверждения</label>
                                    <input type="password" name="password_confirm" id="confirmSignupPassword"
                                           tabindex="6"
                                           class="form-control " placeholder="" autocomplete="off">
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="register-submit" id="register-submit"
                                           tabindex="7" class="form-control btn btn-primary btn-register"
                                           value="Зарегистрировать">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Модальное окно "Вход"-->
<div class="modal" id="loginModal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- заголовок -->
            <div class="modal-header">
                <h3 class="modal-title">Вход</h3>
            </div>

            <!-- Контент формы -->
            <div class="modal-body">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form id="login-form" method="post">
                                <div class="errors"></div>
                                <div class="form-group">
                                    <label class="control-label" for="email">Электронная почта</label>
                                    <input type="email" name="email" id="login_email" tabindex="2"
                                           class="form-control mail_mask"
                                           placeholder="" value="">
                                </div>

                                <div class="form-group">
                                    <label class="control-label" for="password">Пароль</label>
                                    <input type="password" name="password" id="loginPassword" tabindex="5"
                                           class="form-control" placeholder="" autocomplete="off">
                                </div>

                                <div class="form-group">
                                    <input type="submit" name="login-submit" id="login-submit"
                                           tabindex="7" class="form-control btn btn-primary btn-login"
                                           value="Войти">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

</body>
</html>
