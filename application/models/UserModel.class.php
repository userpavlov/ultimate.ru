<?php

// Модель пользователя
class UserModel extends Model
{
    /*
        //Конструктор
        public function __construct($row)
        {
            $this->id = $row['id'];
            $this->username = $row['username'];
            $this->surname = $row['surname'];
            $this->email = $row['email'];
            $this->phone = $row['phone'];
            $this->address = $row['address'];
            $this->password = $row['password'];
        }
    */
    //Имя таблицы
    public static function tableName()
    {
        return 'user';
    }

    //Идентификатор
    public $id;

    //Имя
    public $username;

    //Фамилия
    public $surname;

    //Почта
    public $email;

    //Телефон
    public $phone;

    //Адрес
    public $address;

    //Пароль
    public $password;


    //Сохранить в БД
    public function Save()
    {
        if (empty($this->id)) {

            $query = "INSERT INTO user( username, surname, email, phone, address, password) VALUES (:username, :surname, :email, :phone, :address, :password)";
            $sth = Application::$db->conn->prepare($query);
            $sth->execute(
                array(
                    ':username' => $this->username,
                    ':surname' => $this->surname,
                    ':email' => $this->email,
                    ':phone' => $this->phone,
                    ':address' => $this->address,
                    ':password' => $this->password,
                )
            );

            $result = $sth->fetch(PDO::FETCH_ASSOC);
            return $result["id"];

        } else {

            $query = "update user set username = :username, surname = :surname, email = :email, phone = :phone, address = :address, password = :password where id=:id";
            $sth = Application::$db->conn->prepare($query);
            $sth->execute(
                array(
                    ':username' => $this->username,
                    ':surname' => $this->surname,
                    ':email' => $this->email,
                    ':phone' => $this->phone,
                    ':address' => $this->address,
                    ':password' => $this->password,
                    ':id' => $this->id,
                )
            );

            $result = $sth->fetch(PDO::FETCH_ASSOC);
            return $result["id"];
        }
    }


    //Получить всех пользователей
    public static function getUsers($offset = null, $limit = null)
    {
        $sql = "select * from " . self::tableName();

        if (!is_null($limit)) {
            $sql .= ' LIMIT ' . $limit;
        }

        if (!is_null($offset)) {
            $sql .= ' OFFSET ' . $offset;
        }

        $users = Application::$db->getAll($sql, 'UserModel');
        return $users;
    }


    //Получить пользователя по почте
    public static function getUserByEmail($email)
    {
        $sql = "select * from " . self::tableName() . " where email = :email";
        $user = Application::$db->getOne($sql, 'UserModel', array(":email" => $email));
        return $user;
    }

    //Получить пользователя по идентифиатору
    public static function getUserById($id)
    {
        $sql = "select * from " . self::tableName() . " where id = :id";
        $user = Application::$db->getOne($sql, 'UserModel', array(":id" => $id));
        return $user;
    }


}