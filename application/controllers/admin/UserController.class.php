<?php

//Контроллер
class UserController extends Controller
{
    //index.php
    public function indexAction()
    {
        //Если гость
        if (Application::$user->isGuest()) {
            return $this->render("hello.php", [], !isset($_GET["disable_layout"]) ? 'users_layout.php' : null);
        }

        $this->loader->helper("Pagination");//Загрузка хелпера постраничного просмотра
        //Постраничный просмотр
        $pagination = new PaginationHelper();
        $total = Application::$db->queryScalar("select count(*) from user");
        $pagination->num_pages = ceil(($total / 10));
        $pagination->url = Application::getUrl("admin", "user", "index", ".php");

        if ($_GET) {
            $pagination->active_page = $_GET["page"];
        } else {
            $pagination->active_page = 1;
        }

        $users = UserModel::getUsers(($pagination->active_page - 1) * 10, 10);//Пользователи на странице

        // Загрузить представление
        return $this->render("index.php", ['users' => $users, 'pagination' => $pagination], !isset($_GET["disable_layout"]) ? 'users_layout.php' : null);
    }


    //index.php
    public function viewAction()
    {
        //Если гость
        if (Application::$user->isGuest()) {
            return $this->render("hello.php", [], !isset($_GET["disable_layout"]) ? 'users_layout.php' : null);
        }


        $this->loader->helper("Pagination");//Загрузка хелпера постраничного просмотра

        $user = UserModel::getUserById($_GET["id"]);
        if ($_POST) {
            $user->username = $_POST['username'];
            $user->surname = $_POST['surname'];
            $user->phone = $_POST['phone'];
            $user->address = $_POST['address'];
            $user->Save();
        }

        // Загрузить представление
        return $this->render("view.php", ['user' => $user, 'breadcrumbs' => [$user->username]], 'users_layout.php');
    }

}