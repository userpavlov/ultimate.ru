<?php

//Контроллер
class AuthController extends Controller
{
    //Регистрация
    public function signupAction()
    {
        if ($_POST) {
            $model = new UserModel();
            $model->username = $_POST['username'];
            $model->surname = $_POST['surname'];
            $model->email = $_POST['email'];
            $model->phone = $_POST['phone'];
            $model->address = $_POST['address'];


            $err_code = Application::$user->signup($model);

            $msg = "";
            if ($err_code == -2) {
                echo "Пользователь с данной электронной почтой уже зарегистирован. Введите другую электронную почту.";

            } else if ($err_code == -1) {
                echo  "Не удалось зарегистрировать пользователя.";
            }
            else{
                Application::$user->login($model->email, $_POST['password']);
            }

            echo $msg;

        }
    }


    //Вход
    public function loginAction()
    {
        if ($_POST) {
            $login = Application::$user->login($_POST['email'], $_POST['password']);

            if (!$login) {
                echo "Не удалось войти. Проверьте пользователь и пароль.";
            }
        }
    }

    //Выход
    public function logoutAction()
    {
        Application::$user->logout();
    }
}