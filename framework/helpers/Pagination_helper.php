<?php

//Хелпер постраничного просмотра
class PaginationHelper
{
    //Путь
    public $url;

    //Количество страниц
    public $num_pages;

    //Номер активной страницы
    public $active_page;

    //Постраничный просмотр
    public function getAjaxTags()
    {
        echo "<nav aria-label=\"Page navigation\">
                <ul class=\"pagination\">";

        for ($i = 1; $i <= $this->num_pages; $i++) {
            if ($i == $this->active_page) {
                echo " <li class=\"page-item active\"><a class=\"page-link\" data='" . $i . "' href=\"#\">$i</a></li>";
            } else {
                echo " <li class=\"page-item\"><a class=\"page-link\" data='" . $i . "' href=\"#\">$i</a></li>";
            }
        }

        echo "  </ul></nav>";

        $js = <<<JS
        <script>
$('document').ready(function () {
    $( '.page-item a' ).click( '" . $this->url . "', function(){
        var data = $(this).attr("data");
        $( '#content .container' ).empty().load( "$this->url?page="+ data+"&disable_layout=false");
    })

});
</script>
JS;


        echo $js;
    }
}