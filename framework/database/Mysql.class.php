<?php

// Класс для работы с базами данных
class Mysql {

    public $conn = false;  //Объект соединения

    protected $sql;	//SQL-запрос
   

    /**
     * Конструктор для соединения с базами данных
     * @param $config string Строка конфигурации
     */
    public function __construct($config = array()){
        $host = isset($config['host'])? $config['host'] : 'localhost';//Хост БД
        $user = isset($config['User.class'])? $config['User.class'] : 'root';//Пользователь
        $password = isset($config['password'])? $config['password'] : '';//Пароль
        $dbname = isset($config['dbname'])? $config['dbname'] : '';//Базы данных
        $port = isset($config['port'])? $config['port'] : '3306';//Порт
        $charset = isset($config['charset'])? $config['charset'] : 'utf8';//Кодировка

		//Создаем объект соединения
        try {
            $str = "mysql:host=$host;dbname=$dbname;charset=$charset";
            $this->conn = new PDO($str, $user, $password);
        } catch (\pdoexception $e) {
            echo "database error: " . $e->getmessage();
            die();
        }
        $this->conn->query('set names utf8');
    }



    /**
     * Установка кодировки
     * @access private
     * @param $charset string Кодировка
     */
    private function setChar($charest){
        $sql = 'set names '.$charest;//строка запроса к БД
        $this->query($sql);
    }

    /**
 * Выполнение SQL запроса
 * @access public
 * @param $sql string SQL выражение запроса
 * @return $result， Если успешно, то возвращает ресурс, если ошибка возваращется описание ошибки с выходом
 */
    public function query($sql){

        $sth = $this->conn->prepare($sql);
        $sth->execute();

        $row = $sth->fetch();
        return $row;
    }

    /**
     * Выполнение SQL запроса
     * @access public
     * @param $sql string SQL выражение запроса
     * @return $result， Если успешно, то возвращает ресурс, если ошибка возваращется описание ошибки с выходом
     */
    public function queryScalar($sql){

        $sth = $this->conn->prepare($sql);
        $sth->execute();

        $row = $sth->fetch();
        return count($row)>0 ?  $row[0] : 0;
    }




    /**
     * Получить все записи
     * @access public
     * @param $sql SQL выражение
     * @param $class Класс
     * @return $row Объекты заданного класса
     */
	public function getAll($sql, $class){
        $sth = $this->conn->prepare($sql);
        $sth->execute();

        $row = $sth->fetchAll (PDO::FETCH_CLASS, $class);
        return $row;
    }


    /**
     * Получить 1 запись
     * @access public
     * @param $sql SQL выражение
     * @param $class Класс
     * @return $row Объект заданного класса
     */
    public function getOne($sql, $class, $params = null){
        $sth = $this->conn->prepare($sql);
        $sth->execute($params);

        $row = $sth->fetchAll(PDO::FETCH_CLASS, $class)[0];
        return $row;
    }

}