<?php
//1-й класс фреймворка

class Framework
{

    //При запросах пользователя
    public static function run()
    {
        self::init();//Инициализация констант
        self::autoload();//Автозагрузка классов
        self::dispatch();//Маршрутизация
    }


    //Инициализация констант в ходе запроса к серверу браузером
    private static function init()
    {


        // Определяем константы платформы

        define("DS", DIRECTORY_SEPARATOR);//Разделитель директорий
        define("ROOT", getcwd() . DS);//Рабочий каталог
        define("APP_PATH", ROOT . '..\\' . 'Application' . DS);//Каталог приложения
        define("FRAMEWORK_PATH", ROOT . '..\\' . "framework" . DS);//Путь к каталогу фреймворка
        define("PUBLIC_PATH", ROOT . "public" . DS);//Публичный каталог
        define("CONFIG_PATH", APP_PATH . "config" . DS);//Конфигурационный каталог
        define("CONTROLLER_PATH", APP_PATH . "controllers" . DS);//Путь к контроллерам
        define("MODEL_PATH", APP_PATH . "models" . DS);//Путь к моделям
        define("VIEW_PATH", APP_PATH . "views" . DS);//Путь к представлениям
        define("CORE_PATH", FRAMEWORK_PATH . "core" . DS);//Путь к классам ядра платформы
        define('DB_PATH', FRAMEWORK_PATH . "database" . DS);//Путь к базе данных платформы
        define("LIB_PATH", FRAMEWORK_PATH . "libraries" . DS);//Библиотеки платформы
        define("HELPER_PATH", FRAMEWORK_PATH . "helpers" . DS);//Хелперы платформы
        define("UPLOAD_PATH", PUBLIC_PATH . "uploads" . DS);//Путь к директории автозагрузок


        // Определяем платформу, контроллер, экшен
        if (isset($_GET['route'])) {
            $route = explode("/", $_GET['route']);

            if (count($route) == 3) {
                define("PLATFORM", $route[0]);//Константа на платформу
                define("CONTROLLER", $route[1]);//Константа на контроллер
                define("ACTION", explode('.', $route[2])[0]);//Константа на экшен
            }
        } else {
            define("PLATFORM", 'admin');//Константа на платформу
            define("CONTROLLER", 'user');//Константа на контроллер
            define("ACTION", 'index');//Константа на экшен
        }
        /*else {
            // index.php?p=admin&c=Goods&a=add
            define("PLATFORM", isset($_REQUEST['p']) ? $_REQUEST['p'] : 'admin');//Константа на платформу
            define("CONTROLLER", isset($_REQUEST['c']) ? $_REQUEST['c'] : 'Index');//Константа на контроллер
            define("ACTION", isset($_REQUEST['a']) ? $_REQUEST['a'] : 'index');//Константа на экшен
        }*/


        define("CURR_CONTROLLER_PATH", CONTROLLER_PATH . PLATFORM . DS);//Путь к запрашиваемому запросом контроллеру
        define("CURR_VIEW_PATH", VIEW_PATH . PLATFORM . DS);//Путь к запрашиваемому запросом представлению


        // Загружаем классы ядра core. Они нужны чтобы  обеспечить работу с контроллерами, моджелями, БД, загрузчиком
        require CORE_PATH . "Controller.class.php";//Класс для работы с контроллером
        require CORE_PATH . "Loader.class.php";//Класс для загрузчика
        require CORE_PATH . "User.class.php";//Класс для авторизации пользователя
        require DB_PATH . "Mysql.class.php";//Класс для работы с БД
        require CORE_PATH . "Model.class.php";//Класс для работы с моделями
        require CORE_PATH . "Application.class.php";//Класс для работы с приложением

        // Загружаем конфигурационный файл
        $GLOBALS['config'] = include CONFIG_PATH . "config.php";

        //Восстанавливаем прошлую сессию
        if (!empty($_COOKIE['sid'])) {
            // check session id in cookies
            session_id($_COOKIE['sid']);
        }
        //Стартует новую сессию, либо возобновляет существующую
        session_start();
    }

    //Автозагрузка в самом начале
    private static function autoload()
    {
        spl_autoload_register(array(__CLASS__, 'load'));//Регистируется функция load, которая будет всегда вызываться при создании классов
    }

    /**
     * Для загрузок классов. Происходит при каждом объявлении класса
     * Классы загружаются один раз.
     * Чтобы не вызывать каждый раз require_once, require
     **/
    private static function load($classname)
    {
        // Здесь простая автозагрузка контроллеров и моделей
        //Проверяем постфикс имени класса
        if (substr($classname, -10) == "Controller") {
            // Контроллер
            require_once CURR_CONTROLLER_PATH . "$classname.class.php";
        } elseif (substr($classname, -5) == "Model") {
            // Модель
            require_once MODEL_PATH . "$classname.class.php";
        }
    }


    // Маршрутизация и деспетчеризация (К вызову данного метода уже известны наименования контроллера и экшена)
    private static function dispatch()
    {
        // Создаем контроллер и вызываем его экшен
        $controller_name = CONTROLLER . "Controller";//Имя класса контроллера
        $action_name = ACTION . "Action";//Имя экшена
        $controller = new $controller_name;//Создаем объект контроллера
        echo $controller->$action_name();//Вызываем экшен контроллера
    }


}