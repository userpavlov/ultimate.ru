<?php

//Пользователь
class User
{
    //Здесть хранится авторизированный пользователь
    public $identity;


    public function __construct($username = null, $password = null)
    {
        if(!$this->isGuest()){
            $this->identity = UserModel::getUserById($_SESSION["user_id"]);
        }

    }

    //Гость
    public function isGuest()
    {
        if (!empty($_SESSION["user_id"])) {
            return !(bool)$_SESSION["user_id"];
        }
        return true;
    }

    //Регистрация пользователя
    public function signup(UserModel $model)
    {
        $user = UserModel::getUserByEmail($model->email);

        //Получить пользователя по почте
        if ($user != null) {
            return -2;
        }

        //хеш пароля(простой алгоритм без соли)
        $model->password = password_hash($_POST['password'], PASSWORD_BCRYPT, array('cost' => 11));

        try {
            if ($model->save() >= 0) {
                $this->identity = $model;
            }
        } catch (PDOException $e) {
            return -1;
        }
    }

    //Вход
    public function login($email, $password, $remember = true)
    {
        $user = UserModel::getUserByEmail($email);

        if ($user) {
            //Верификация пароля
            if (password_verify($password, $user->password)) {
                $this->identity = $user;
                $this->saveSession($remember);
            }
        }

        return !$this->isGuest();
    }

    public function logout()
    {
        if (!empty($_SESSION["user_id"])) {
            unset($_SESSION["user_id"]);
        }

        $this->identity = null;
    }

    //Сохранить сессию в cookie
    public function saveSession($remember = false, $http_only = true, $days = 7)
    {
        $_SESSION["user_id"] = $this->identity->id;

        if ($remember) {
            // Save session id in cookies
            $sid = session_id();

            $expire = time() + $days * 24 * 3600;
            $domain = ""; // default domain
            $secure = false;
            $path = "/";

            setcookie("sid", $sid, $expire, $path, $domain, $secure, $http_only);//Сохраняем идентификатор сессии
        }
    }


}