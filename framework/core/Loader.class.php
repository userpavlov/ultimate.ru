<?php
class Loader{

    // Загружает библиотечные классы
    public function library($lib){
        include LIB_PATH . "$lib.class.php";
    }

    // Загружает функции helper: файлы с постфиксом  _helper.php;
    public function helper($helper){
        include HELPER_PATH . "{$helper}_helper.php";
    }
}