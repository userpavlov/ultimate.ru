<?php

//Приложение
class Application
{
    //Объект соединения с БД
    public static $db;

    //Для авторизации
    public static $user;


    //Получить URL для запроса
    public static function getUrl($platform, $controller, $action, $ext = "")
    {
        return "/" . $platform . "/" . $controller . "/" . $action . $ext;
    }
}