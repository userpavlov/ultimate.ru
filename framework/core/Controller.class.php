<?php

// Базовый контроллер
class Controller{


	
	//Объект загрузчика
	protected $loader;


    public function __construct(){
        $this->loader = new Loader(); //Подгружаем библиотеки и хелперы	
		
		//Конфигурации определены в файле config.php
		//Настройки для соединения с БД
		$dbconfig['host'] = $GLOBALS['config']['host'];
        $dbconfig['user'] = $GLOBALS['config']['user'];
        $dbconfig['password'] = $GLOBALS['config']['password'];
        $dbconfig['dbname'] = $GLOBALS['config']['dbname'];
        $dbconfig['port'] = $GLOBALS['config']['port'];
        $dbconfig['charset'] = $GLOBALS['config']['charset'];

        Application::$db = new Mysql($dbconfig);
        Application::$user = new User();
    }


	//Редирект
    public function redirect($url,$message,$wait = 0){

        if ($wait == 0){

            header("Location:$url");

        } else {

            include CURR_VIEW_PATH . "message.html";

        }

        exit;

    }
	
	/**Рендеринг
	**	@view string Файл представления
	**	@data array Данные
	**	@layout string Шаблон
	**  @return Результат рендернига
	**/
	function render($view, $data = null, $layout = null) {
	    $view = CONTROLLER . "\\" . $view;
		//Распаковываем в текущую директорию
		if (!is_null($data)){
			extract($data);
		}
		
		if(is_null($layout)){
			//Рендеринг без шаблона
			ob_start();
			include  CURR_VIEW_PATH . $view;
			return ob_get_clean();
		}
		else {
			//Рендеринг с шаблоном
			ob_start();
			include  CURR_VIEW_PATH . $view;
			$content = ob_get_clean();
			
			ob_start();
			include  CURR_VIEW_PATH . 'layouts\\' .$layout;
			return ob_get_clean();			
		}		
	}

}